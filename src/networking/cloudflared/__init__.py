import pulumi

from .daemonset import cloudflared_daemonset
from .tunnel import cloudflared_tunnel

config = pulumi.Config('homelab')
domain = config.require('primary-domain')

# Create Tunnel
tunnel = cloudflared_tunnel(domain)
deployment = cloudflared_daemonset(tunnel_token=tunnel.tunnel_token)
tunnel_domain = tunnel.cname
