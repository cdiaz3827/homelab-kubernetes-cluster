import pulumi
import pulumi_cloudflare as cloudflare
import pulumi_random as random

project = pulumi.get_project()
stack = pulumi.get_stack()


def cloudflared_tunnel(zone_name: str):
    """
    Creates a Cloudflare Tunnel for the given zone.

    Args:
        zone_name: The name of the Cloudflare zone to create the tunnel for.
    Returns:
        The Cloudflare Tunnel resource.
    """
    zone = cloudflare.get_zone(name=zone_name)
    tunnel_secret = random.RandomBytes('cloudflare-tunnel-secret', length=32)
    tunnel = cloudflare.Tunnel(
        f'{project}-{stack}-cloudflare-tunnel',
        name=f'{project}-{stack}-tunnel',
        account_id=zone.account_id,
        secret=tunnel_secret.base64,
        config_src='cloudflare',
    )
    config = cloudflare.TunnelConfig(
        f'{project}-{stack}-cloudflare-tunnel-config',
        account_id=tunnel.account_id,
        tunnel_id=tunnel.id,
        config=cloudflare.TunnelConfigConfigArgs(
            ingress_rules=[
                cloudflare.TunnelConfigConfigIngressRuleArgs(
                    hostname='traefik',
                    service='http://traefik.traefik:80',
                ),
                cloudflare.TunnelConfigConfigIngressRuleArgs(
                    hostname='sso.cdiaz.cloud',
                    service='http://authentik-server.authentik.svc.homelab.cdiaz.cloud:80',
                ),
                cloudflare.TunnelConfigConfigIngressRuleArgs(
                    service='http://traefik.traefik:80',
                ),
            ],
        ),
        opts=pulumi.ResourceOptions(parent=tunnel),
    )
    pulumi.export('cloudflare-tunnel-token', tunnel.tunnel_token)
    pulumi.export('cloudflare-tunnel-cname', tunnel.cname)
    return tunnel
