import pulumi
import pulumi_kubernetes as kubernetes

# Retrieve Vars
config = pulumi.Config('cloudflared')
replicas = config.get_int('replicas', 2)


def cloudflared_daemonset(tunnel_token: str):
    labels = {
        'app': 'cloudflared',
    }
    namespace = kubernetes.core.v1.Namespace(
        'cloudflared',
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name='cloudflared',
        ),
    )
    common_metadata = kubernetes.meta.v1.ObjectMetaArgs(
        name='cloudflared',
        namespace=namespace.metadata.name,
        labels=labels,
    )
    secret = kubernetes.core.v1.Secret(
        'cloudflared-secret',
        metadata=common_metadata,
        string_data={
            'TUNNEL_TOKEN': tunnel_token,
        },
        opts=pulumi.ResourceOptions(parent=namespace),
    )
    daemonset = kubernetes.apps.v1.DaemonSet(
        'cloudflared-daemonset',
        metadata=common_metadata,
        spec=kubernetes.apps.v1.DaemonSetSpecArgs(
            selector=kubernetes.meta.v1.LabelSelectorArgs(
                match_labels=labels,
            ),
            template=(
                kubernetes.core.v1.PodTemplateSpecArgs(
                    metadata=kubernetes.meta.v1.ObjectMetaArgs(
                        labels=labels,
                    ),
                    spec=kubernetes.core.v1.PodSpecArgs(
                        containers=[
                            kubernetes.core.v1.ContainerArgs(
                                name='cloudflared',
                                image='cloudflare/cloudflared:latest',
                                command=[
                                    'cloudflared',
                                    'tunnel',
                                    '--metrics',
                                    '0.0.0.0:2000',
                                    'run',
                                ],
                                ports=[
                                    kubernetes.core.v1.ContainerPortArgs(
                                        name='metrics',
                                        container_port=2000,
                                    )
                                ],
                                liveness_probe=kubernetes.core.v1.ProbeArgs(
                                    http_get=kubernetes.core.v1.HTTPGetActionArgs(
                                        path='/ready',
                                        port='metrics',
                                    ),
                                    failure_threshold=1,
                                    initial_delay_seconds=10,
                                    period_seconds=10,
                                ),
                                env_from=[
                                    kubernetes.core.v1.EnvFromSourceArgs(
                                        secret_ref=kubernetes.core.v1.SecretEnvSourceArgs(
                                            name=secret.metadata.name,
                                        )
                                    ),
                                ],
                            )
                        ],
                    ),
                )
            ),
        ),
        opts=pulumi.ResourceOptions(parent=namespace),
    )
    return daemonset
