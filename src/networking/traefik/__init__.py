import pulumi

from .traefik import Traefik, TraefikArgs

config = pulumi.Config('traefik')
enabled = config.get_bool('enabled')


if enabled:
    traefik = Traefik(
        'traefik',
        TraefikArgs(namespace='traefik'),
    )
else:
    pulumi.export('Traefik Status:', value='Disabled')
