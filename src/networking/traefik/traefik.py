from pulumi import ComponentResource, Output, ResourceOptions, export
from pulumi_cloudflare import Record
from pulumi_kubernetes.helm.v3 import Release, ReleaseArgs, RepositoryOptsArgs

from modules.cloudflare import TunnelIngress, TunnelIngressArgs


class TraefikArgs:
    def __init__(self, namespace: str, version: str = 'v28.3.0'):
        self.namespace = namespace
        self.version = version


class Traefik(ComponentResource):
    def __init__(
        self, name: str, args: TraefikArgs, opts: ResourceOptions = None
    ):
        super().__init__('networking:traefik', name, {}, opts)

        self.traefik = Release(
            f'{name}_release',
            ReleaseArgs(
                chart='traefik',
                name='traefik',
                repository_opts=RepositoryOptsArgs(
                    repo='https://traefik.github.io/charts',
                ),
                version=args.version,
                namespace=args.namespace,
                create_namespace=True,
                values={
                    'deployment': {'kind': 'DaemonSet'},
                    'service': {
                        'type': 'ClusterIP',
                    },
                    'ingressRoute': {
                        'dashboard': {
                            'enabled': 'true',
                            'matchRule': 'Host(`traefik.cdiaz.cloud`)',
                            'entryPoints': ['web'],
                        }
                    },
                },
            ),
            opts=ResourceOptions(parent=self),
        )
        self.dns = TunnelIngress(
            f'{name}_dns',
            args=TunnelIngressArgs(hostname='traefik.cdiaz.cloud'),
            opts=ResourceOptions(parent=self),
        )
        self.register_outputs({})
