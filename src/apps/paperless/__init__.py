import pulumi

from .paperless import Paperless, PaperlessArgs

config = pulumi.Config('paperless')
enabled = config.get_bool('enabled')
version = config.get('version', default='latest')
hostname = config.get('hostname')


if enabled:
    paperless = Paperless(
        'paperless',
        PaperlessArgs(
            version=version,
            hostname=hostname,
            oidc_client_id=config.require('oidc_client_id'),
            oidc_client_secret=config.require('oidc_client_secret'),
        ),
    )
