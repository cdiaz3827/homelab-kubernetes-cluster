import json

import pulumi_kubernetes as kubernetes
from pulumi import ComponentResource, Config, ResourceOptions, export
from pulumi_random import RandomBytes

from modules.cloudflare import TunnelIngress, TunnelIngressArgs
from modules.db import PostgresArgs, PostgresDatabase
from modules.redis import Redis, RedisArgs

from .gotenberg import Gotenberg, GotenbergArgs
from .tika import Tika, TikaArgs

db_config = Config('postgresql')
db_host = db_config.get('host')
smtp = Config('smtp')


class PaperlessArgs:
    def __init__(
        self,
        version: str,
        hostname: str,
        paperless_port: int = 8000,
        enable_gotenberg: bool = True,
        enable_tika: bool = True,
        oidc_client_id: str = None,
        oidc_client_secret: str = None,
    ):
        self.version = version
        self.hostname = hostname
        self.paperless_port = paperless_port
        self.enable_gotenberg = enable_gotenberg
        self.enable_tika = enable_tika
        self.oidc_client_id = oidc_client_id
        self.oidc_client_secret = oidc_client_secret


class Paperless(ComponentResource):
    def __init__(
        self, name: str, args: PaperlessArgs, opts: ResourceOptions = None
    ):
        super().__init__('apps:paperless', name, {}, opts)

        self.namespace = kubernetes.core.v1.Namespace(
            name + '_namespace',
            metadata={'name': name},
            opts=ResourceOptions(parent=self),
        )

        self.redis = Redis(
            'paperless_redis',
            RedisArgs(
                namespace=self.namespace.metadata.name,
            ),
            opts=ResourceOptions(parent=self.namespace),
        )

        self.db = PostgresDatabase(
            f'{name}_database',
            args=PostgresArgs(db_name='paperless', db_username='paperless'),
            opts=ResourceOptions(parent=self),
        )
        if args.enable_gotenberg:
            self.gottenberg = Gotenberg(
                f'{name}_gotenberg',
                args=GotenbergArgs(namespace=self.namespace.metadata.name),
                opts=ResourceOptions(parent=self.namespace),
            )

        if args.enable_tika:
            self.tika = Tika(
                f'{name}_tika',
                args=TikaArgs(
                    namespace=self.namespace.metadata.name,
                ),
                opts=ResourceOptions(parent=self.namespace),
            )

        labels = {'app': 'paperless', 'tier': 'webserver'}
        metadata = kubernetes.meta.v1.ObjectMetaArgs(
            name=name, labels=labels, namespace=self.namespace.metadata.name
        )

        secret_key = RandomBytes(
            f'{name}_secret_key',
            length=60,
            opts=ResourceOptions(parent=self.namespace),
        )

        self.pvc = kubernetes.core.v1.PersistentVolumeClaim(
            f'{name}_pvc',
            metadata=metadata,
            spec=kubernetes.core.v1.PersistentVolumeClaimSpecArgs(
                access_modes=['ReadWriteOnce'],
                storage_class_name='nfs-csi',
                resources=kubernetes.core.v1.ResourceRequirementsArgs(
                    requests={'storage': '1Gi'}
                ),
            ),
            opts=ResourceOptions(parent=self.namespace),
        )

        self.config = kubernetes.core.v1.ConfigMap(
            f'{name}_config',
            metadata=metadata,
            data={
                'PAPERLESS_URL': f'https://{args.hostname}',
                'USERMAP_UID': '99',
                'USERMAP_GID': '100',
                'PAPERLESS_PORT': str(args.paperless_port),
                'PAPERLESS_TIKA_ENABLED': str(args.enable_tika),
                'PAPERLESS_TIKA_ENDPOINT': self.tika.endpoint,
                'PAPERLESS_GOTENBERG_ENABLED': str(args.enable_gotenberg),
                'PAPERLESS_GOTENBERG_ENDPOINT': self.gottenberg.endpoint,
                'PAPERLESS_DISABLE_REGULAR_LOGIN': 'True',
            },
            opts=ResourceOptions(parent=self.namespace),
        )

        self.secret = kubernetes.core.v1.Secret(
            f'{name}_secret',
            metadata=metadata,
            string_data={
                'PAPERLESS_SECRET_KEY': secret_key.base64,
                'PAPERLESS_REDIS': self.redis.connection_string,
                'PAPERLESS_DBENGINE': 'postgresql',
                'PAPERLESS_DBHOST': db_host,
                'PAPERLESS_DBPORT': str(5432),
                'PAPERLESS_DBNAME': self.db.database_name,
                'PAPERLESS_DBUSER': self.db.username,
                'PAPERLESS_DBPASS': self.db.password,
                'PAPERLESS_EMAIL_HOST': smtp.get('host'),
                'PAPERLESS_EMAIL_PORT': str(smtp.get('port')),
                'PAPERLESS_EMAIL_HOST_USER': smtp.get('username'),
                'PAPERLESS_EMAIL_HOST_PASSWORD': smtp.get_secret('token'),
                'PAPERLESS_EMAIL_USE_TLS': 'True',
                'PAPERLESS_APPS': 'allauth.socialaccount.providers.openid_connect',
                'PAPERLESS_SOCIALACCOUNT_PROVIDERS': json.dumps(
                    {
                        'openid_connect': {
                            'APPS': [
                                {
                                    'provider_id': 'authentik',
                                    'name': 'Authentik',
                                    'client_id': args.oidc_client_id,
                                    'secret': args.oidc_client_secret,
                                    'settings': {
                                        'server_url': 'https://sso.cdiaz.cloud/application/o/paperless/.well-known/openid-configuration'
                                    },
                                }
                            ],
                            'OAUTH_PKCE_ENABLED': 'True',
                        }
                    }
                ),
            },
            opts=ResourceOptions(parent=self.namespace),
        )

        self.deployment = kubernetes.apps.v1.Deployment(
            f'{name}_deployment',
            metadata=metadata,
            spec=kubernetes.apps.v1.DeploymentSpecArgs(
                replicas=1,
                selector=kubernetes.meta.v1.LabelSelectorArgs(
                    match_labels=labels
                ),
                template=kubernetes.core.v1.PodTemplateSpecArgs(
                    metadata=kubernetes.meta.v1.ObjectMetaArgs(labels=labels),
                    spec=kubernetes.core.v1.PodSpecArgs(
                        containers=[
                            kubernetes.core.v1.ContainerArgs(
                                name='paperless',
                                image='ghcr.io/paperless-ngx/paperless-ngx:latest',
                                ports=[
                                    kubernetes.core.v1.ContainerPortArgs(
                                        name='http',
                                        container_port=args.paperless_port,
                                    )
                                ],
                                env_from=[
                                    kubernetes.core.v1.EnvFromSourceArgs(
                                        config_map_ref=kubernetes.core.v1.ConfigMapEnvSourceArgs(
                                            name=self.config.metadata.name,
                                            optional=False,
                                        )
                                    ),
                                    kubernetes.core.v1.EnvFromSourceArgs(
                                        secret_ref=kubernetes.core.v1.SecretEnvSourceArgs(
                                            name=self.secret.metadata.name,
                                            optional=False,
                                        )
                                    ),
                                ],
                                volume_mounts=[
                                    kubernetes.core.v1.VolumeMountArgs(
                                        name='paperless',
                                        mount_path='/usr/src/paperless/data',
                                        sub_path='data',
                                    ),
                                    kubernetes.core.v1.VolumeMountArgs(
                                        name='paperless',
                                        mount_path='/usr/src/paperless/media',
                                        sub_path='media',
                                    ),
                                    kubernetes.core.v1.VolumeMountArgs(
                                        name='paperless',
                                        mount_path='/usr/src/paperless/export',
                                        sub_path='export',
                                    ),
                                    kubernetes.core.v1.VolumeMountArgs(
                                        name='paperless',
                                        mount_path='/usr/src/paperless/consume',
                                        sub_path='consume',
                                    ),
                                ],
                            )
                        ],
                        volumes=[
                            kubernetes.core.v1.VolumeArgs(
                                name='paperless',
                                persistent_volume_claim=kubernetes.core.v1.PersistentVolumeClaimVolumeSourceArgs(
                                    claim_name=self.pvc.metadata.name
                                ),
                            )
                        ],
                    ),
                ),
            ),
            opts=ResourceOptions(
                parent=self.namespace,
                depends_on=[
                    self.pvc,
                    self.redis,
                    self.db,
                    self.tika,
                    self.gottenberg,
                ],
            ),
        )

        self.service = kubernetes.core.v1.Service(
            f'{name}_service',
            metadata=metadata,
            spec=kubernetes.core.v1.ServiceSpecArgs(
                type='ClusterIP',
                selector=labels,
                ports=[
                    kubernetes.core.v1.ServicePortArgs(
                        name='http',
                        port=args.paperless_port,
                        target_port='http',
                    ),
                ],
            ),
            opts=ResourceOptions(
                parent=self.namespace,
                depends_on=[self.deployment],
            ),
        )
        metadata.annotations = {'pulumi.com/skipAwait': 'true'}
        self.ingress = kubernetes.networking.v1.Ingress(
            f'{name}_ingress',
            metadata=metadata,
            spec=kubernetes.networking.v1.IngressSpecArgs(
                ingress_class_name='traefik',
                rules=[
                    kubernetes.networking.v1.IngressRuleArgs(
                        host=args.hostname,
                        http=kubernetes.networking.v1.HTTPIngressRuleValueArgs(
                            paths=[
                                kubernetes.networking.v1.HTTPIngressPathArgs(
                                    path='/',
                                    path_type='Prefix',
                                    backend=kubernetes.networking.v1.IngressBackendArgs(
                                        service=kubernetes.networking.v1.IngressServiceBackendArgs(
                                            name=self.service.metadata.name,
                                            port=kubernetes.networking.v1.ServiceBackendPortArgs(
                                                name='http',
                                            ),
                                        )
                                    ),
                                ),
                            ],
                        ),
                    ),
                ],
            ),
            opts=ResourceOptions(
                parent=self.service,
                depends_on=[self.service],
            ),
        )
        self.dns_record = TunnelIngress(
            f'{name}_dns',
            args=TunnelIngressArgs(hostname=args.hostname),
            opts=ResourceOptions(parent=self.ingress),
        )
        self.register_outputs({})
