import pulumi_kubernetes as kubernetes
from pulumi import ComponentResource, Output, ResourceOptions

port = 9998


class TikaArgs:
    def __init__(self, namespace: str):
        self.namespace = namespace


class Tika(ComponentResource):
    def __init__(
        self, name: str, args: TikaArgs, opts: ResourceOptions = None
    ):
        super().__init__('service:tika', name, {}, opts)

        labels = {'app': 'paperless', 'component': 'Tika'}

        common_metadata = kubernetes.meta.v1.ObjectMetaArgs(
            name='tika',
            namespace=args.namespace,
            labels=labels,
        )

        self.deployment = kubernetes.apps.v1.Deployment(
            f'{name}_deployment',
            metadata=common_metadata,
            spec=kubernetes.apps.v1.DeploymentSpecArgs(
                replicas=1,
                selector=kubernetes.meta.v1.LabelSelectorArgs(
                    match_labels=labels
                ),
                template=kubernetes.core.v1.PodTemplateSpecArgs(
                    metadata=common_metadata,
                    spec=kubernetes.core.v1.PodSpecArgs(
                        containers=[
                            kubernetes.core.v1.ContainerArgs(
                                name='tika',
                                image='docker.io/apache/tika:latest',
                                ports=[
                                    kubernetes.core.v1.ContainerPortArgs(
                                        container_port=port
                                    )
                                ],
                                security_context=kubernetes.core.v1.SecurityContextArgs(
                                    run_as_user=99,
                                    run_as_group=100,
                                    privileged=False,
                                ),
                            ),
                        ],
                    ),
                ),
            ),
            opts=ResourceOptions(parent=self),
        )

        self.service = kubernetes.core.v1.Service(
            f'{name}_service',
            metadata=common_metadata,
            spec=kubernetes.core.v1.ServiceSpecArgs(
                type='ClusterIP',
                selector=labels,
                ports=[
                    kubernetes.core.v1.ServicePortArgs(
                        port=port,
                        target_port=port,
                    )
                ],
            ),
            opts=ResourceOptions(parent=self.deployment),
        )
        self.endpoint = Output.concat(
            'http://',
            self.service.metadata.name,
            '.',
            self.service.metadata.namespace,
            ':',
            'port',
        )

        self.register_outputs({'endpoint': self.endpoint})
