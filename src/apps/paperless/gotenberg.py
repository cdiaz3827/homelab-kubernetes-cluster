import pulumi_kubernetes as kubernetes
from pulumi import ComponentResource, Output, ResourceOptions


class GotenbergArgs:
    def __init__(self, namespace: str):
        self.namespace = namespace


class Gotenberg(ComponentResource):
    def __init__(
        self, name: str, args: GotenbergArgs, opts: ResourceOptions = None
    ):
        super().__init__('service:gotenberg', name, {}, opts)

        labels = {'app': 'paperless', 'component': 'gotenberg'}

        common_metadata = kubernetes.meta.v1.ObjectMetaArgs(
            name='gotenberg',
            namespace=args.namespace,
            labels=labels,
        )

        self.deployment = kubernetes.apps.v1.Deployment(
            f'{name}_deployment',
            metadata=common_metadata,
            spec=kubernetes.apps.v1.DeploymentSpecArgs(
                replicas=1,
                selector=kubernetes.meta.v1.LabelSelectorArgs(
                    match_labels=labels
                ),
                template=kubernetes.core.v1.PodTemplateSpecArgs(
                    metadata=common_metadata,
                    spec=kubernetes.core.v1.PodSpecArgs(
                        containers=[
                            kubernetes.core.v1.ContainerArgs(
                                name='gotenberg',
                                image='docker.io/gotenberg/gotenberg:7.10',
                                command=['gotenberg'],
                                args=[
                                    '--chromium-disable-javascript=true',
                                    '--chromium-allow-list=file:///tmp/.*',
                                ],
                                ports=[
                                    kubernetes.core.v1.ContainerPortArgs(
                                        container_port=3000
                                    )
                                ],
                                resources=kubernetes.core.v1.ResourceRequirementsArgs(
                                    requests={'cpu': '0.2', 'memory': '256Mi'},
                                    limits={'cpu': '0.2', 'memory': '512Mi'},
                                ),
                                security_context=kubernetes.core.v1.SecurityContextArgs(
                                    run_as_user=99,
                                    run_as_group=100,
                                    privileged=False,
                                ),
                            ),
                        ],
                    ),
                ),
            ),
            opts=ResourceOptions(parent=self),
        )

        self.service = kubernetes.core.v1.Service(
            f'{name}_service',
            metadata=common_metadata,
            spec=kubernetes.core.v1.ServiceSpecArgs(
                type='ClusterIP',
                selector=labels,
                ports=[
                    kubernetes.core.v1.ServicePortArgs(
                        port=3000,
                        target_port=3000,
                    )
                ],
            ),
            opts=ResourceOptions(parent=self.deployment),
        )

        self.endpoint = Output.concat(
            'http://',
            self.service.metadata.name,
            '.',
            self.service.metadata.namespace,
            ':',
            '3000',
        )

        self.register_outputs({'endpoint': self.endpoint})
