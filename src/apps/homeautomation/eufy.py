from pulumi import ComponentResource, Output, ResourceOptions, export
from pulumi_kubernetes.apps.v1 import (
    Deployment,
    DeploymentArgs,
    DeploymentSpecArgs,
)
from pulumi_kubernetes.core.v1 import (
    ContainerArgs,
    ContainerPortArgs,
    EnvFromSourceArgs,
    PersistentVolumeClaim,
    PersistentVolumeClaimArgs,
    PersistentVolumeClaimSpecArgs,
    PersistentVolumeClaimVolumeSourceArgs,
    PodSpecArgs,
    PodTemplateSpecArgs,
    Secret,
    SecretArgs,
    SecretEnvSourceArgs,
    SecretInitArgs,
    SecurityContextArgs,
    Service,
    ServicePortArgs,
    ServiceSpecArgs,
    VolumeArgs,
    VolumeMountArgs,
)
from pulumi_kubernetes.meta.v1 import LabelSelectorArgs, ObjectMetaArgs


class EufyArgs:
    def __init__(
        self,
        namespace: str,
        version_tag: str,
        username: str,
        password: str,
        trusted_device_name: str = None,
        listening_port: int = 3000,
        node_port: int = 3000,
    ):
        self.namespace = namespace
        self.version_tag = version_tag
        self.username = username
        self.password = password
        self.trusted_device_name = trusted_device_name
        self.listening_port = listening_port
        self.node_port = node_port


class EufyBridge(ComponentResource):
    def __init__(
        self,
        name: str,
        args: EufyArgs,
        opts: ResourceOptions = None,
    ):
        super().__init__('apps:eufy-bridge', name, {}, opts)

        labels = {
            'app': 'eufy-bridge',
        }

        child_opts = ResourceOptions(parent=self)

        common_metadata = ObjectMetaArgs(
            name='eufy-bridge',
            namespace=args.namespace,
            labels=labels,
        )

        self.secret = Secret(
            f'{name}_secret',
            metadata=common_metadata,
            string_data={
                'USERNAME': args.username,
                'PASSWORD': args.password,
                'COUNTRY': 'US',
                'LANGUAGE': 'en',
                'PORT': str(args.listening_port),
            },
            opts=child_opts,
        )

        self.pvc = PersistentVolumeClaim(
            f'{name}_pvc',
            metadata=common_metadata,
            spec=PersistentVolumeClaimSpecArgs(
                access_modes=['ReadWriteOnce'],
                resources={
                    'requests': {
                        'storage': '100Mi',
                    },
                },
                storage_class_name='nfs-csi',
            ),
            opts=child_opts,
        )

        self.deployment = Deployment(
            f'{name}_deployment',
            metadata=common_metadata,
            spec=DeploymentSpecArgs(
                replicas=1,
                selector=LabelSelectorArgs(match_labels=labels),
                template=PodTemplateSpecArgs(
                    metadata=ObjectMetaArgs(
                        labels=labels,
                    ),
                    spec=PodSpecArgs(
                        host_network=True,
                        containers=[
                            ContainerArgs(
                                name='eufy-bridge',
                                image=f'bropat/eufy-security-ws:{args.version_tag}',
                                ports=[
                                    ContainerPortArgs(
                                        container_port=args.listening_port,
                                    ),
                                ],
                                env_from=[
                                    EnvFromSourceArgs(
                                        secret_ref=SecretEnvSourceArgs(
                                            name=self.secret.metadata.name,
                                            optional=False,
                                        )
                                    )
                                ],
                                volume_mounts=[
                                    VolumeMountArgs(
                                        name='eufy',
                                        sub_path='eufy',
                                        mount_path='/data',
                                    )
                                ],
                                security_context=SecurityContextArgs(
                                    privileged=True,
                                    run_as_user=99,
                                    run_as_group=100,
                                ),
                            ),
                        ],
                        volumes=[
                            VolumeArgs(
                                name='eufy',
                                persistent_volume_claim=PersistentVolumeClaimVolumeSourceArgs(
                                    claim_name=self.pvc.metadata.name,
                                ),
                            ),
                        ],
                    ),
                ),
            ),
            opts=child_opts,
        )

        self.service = Service(
            f'{name}_service',
            metadata=common_metadata,
            spec=ServiceSpecArgs(
                selector=labels,
                type='ClusterIP',
                ports=[
                    ServicePortArgs(
                        port=args.node_port,
                        target_port=args.listening_port,
                        protocol='TCP',
                    )
                ],
            ),
            opts=child_opts,
        )

        self.endpoint = Output.concat(
            'http://',
            self.service.metadata.name,
            '.',
            self.service.metadata.namespace,
            ':',
            str(args.node_port),
        )

        export(name='Eufy Bridge Endpoint', value=self.endpoint)

        self.register_outputs({'endpoint': self.endpoint})
