from pulumi import ComponentResource, Config, ResourceOptions

from modules.namespace import K8sNamespace
from modules.pv import nfs_pv

from .eufy import EufyArgs, EufyBridge
from .hass import Hass, HassArgs

nfs_config = Config('nfs')
nfs_server = nfs_config.get('server')

eufy_config = Config('eufy')
eufy_version = eufy_config.get('version', default='latest')
eufy_username = eufy_config.get('username')
eufy_password = eufy_config.get_secret('password')


class HomeAutomationArgs:
    def __init__(
        self,
        hass_version: str,
        nfs_path: str,
    ):
        self.hass_version = hass_version
        self.nfs_path = nfs_path


class HomeAutomation(ComponentResource):
    def __init__(
        self,
        name: str,
        args: HomeAutomationArgs,
        opts: ResourceOptions = None,
    ):
        super().__init__('stacks:homeautomation', name, {}, opts)

        self.ns = K8sNamespace(
            name='homeautomation', opts=ResourceOptions(parent=self)
        )

        self.pv = nfs_pv(
            name='home-automation',
            nfs_server=nfs_server,
            nfs_path=args.nfs_path,
            opts=ResourceOptions(parent=self),
        )

        self.eufy = EufyBridge(
            f'{name}_eufy',
            args=EufyArgs(
                namespace=self.ns.metadata.name,
                version_tag=eufy_version,
                username=eufy_username,
                password=eufy_password,
            ),
            opts=ResourceOptions(parent=self.ns),
        )

        self.hass = Hass(
            f'{name}_hass',
            args=HassArgs(
                namespace=self.ns.metadata.name,
                subdomain='homeassistant',
                pv_name=self.pv.metadata.name,
                version=args.hass_version,
            ),
            opts=ResourceOptions(parent=self.ns),
        )

        self.register_outputs({})
