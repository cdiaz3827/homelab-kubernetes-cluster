from pulumi import Config

from .homeautomation import HomeAutomation, HomeAutomationArgs

config = Config('homeautomation')
enabled = config.get_bool('enabled', False)

hass_config = Config('hass')
version = hass_config.get('version', default='latest')
nfs_path = hass_config.get('nfs_path')

if enabled:
    HomeAutomation(
        'homeautomation',
        args=HomeAutomationArgs(hass_version=version, nfs_path=nfs_path),
    )
