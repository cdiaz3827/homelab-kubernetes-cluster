from pulumi import ComponentResource, ResourceOptions
from pulumi_kubernetes.apps.v1 import Deployment, DeploymentSpecArgs
from pulumi_kubernetes.core.v1 import (
    ContainerArgs,
    ContainerPortArgs,
    HostPathVolumeSourceArgs,
    PersistentVolumeClaim,
    PersistentVolumeClaimSpecArgs,
    PersistentVolumeClaimVolumeSourceArgs,
    PodSpecArgs,
    PodTemplateSpecArgs,
    ResourceRequirementsArgs,
    SecurityContextArgs,
    Service,
    ServicePortArgs,
    ServiceSpecArgs,
    VolumeArgs,
    VolumeMountArgs,
)
from pulumi_kubernetes.meta.v1 import LabelSelectorArgs, ObjectMetaArgs
from pulumi_kubernetes.networking.v1 import (
    HTTPIngressPathArgs,
    HTTPIngressRuleValueArgs,
    Ingress,
    IngressBackendArgs,
    IngressRuleArgs,
    IngressServiceBackendArgs,
    IngressSpecArgs,
    ServiceBackendPortArgs,
)

from modules.cloudflare import TunnelIngress, TunnelIngressArgs
from modules.ingress import K8sIngress


class HassArgs:
    def __init__(
        self,
        namespace: str,
        subdomain: str,
        version: str,
        pv_name: str,
        storage_request: str = '1Gi',
        memory_request: str = '256Mi',
    ):
        self.namespace = namespace
        self.version = version
        self.pv_name = pv_name
        self.memory_request = memory_request
        self.storage_request = storage_request
        self.subdomain = subdomain


class Hass(ComponentResource):
    def __init__(
        self,
        name: str,
        args: HassArgs,
        opts: ResourceOptions = None,
    ):
        super().__init__('apps:hass', name, {}, opts)

        labels = {
            'app': 'hass',
            'component': 'hass',
            'version': args.version,
        }

        hostname = f'{args.subdomain}.cdiaz.cloud'

        self.pvc = PersistentVolumeClaim(
            f'{name}_pvc',
            metadata=ObjectMetaArgs(
                name='home-assistant',
                namespace=args.namespace,
                labels=labels,
            ),
            spec=PersistentVolumeClaimSpecArgs(
                access_modes=['ReadWriteOnce'],
                resources={
                    'requests': {
                        'storage': args.storage_request,
                    },
                },
                volume_name=args.pv_name,
                storage_class_name='nfs-csi',
            ),
        )

        self.deployment = Deployment(
            f'{name}_deployment',
            metadata=ObjectMetaArgs(
                name='home-assistant',
                namespace=args.namespace,
                labels=labels,
            ),
            spec=DeploymentSpecArgs(
                replicas=1,
                selector=LabelSelectorArgs(match_labels=labels),
                template=PodTemplateSpecArgs(
                    metadata=ObjectMetaArgs(
                        labels=labels,
                    ),
                    spec=PodSpecArgs(
                        host_network=True,
                        node_selector={
                            'cdiaz.cloud/bluez': 'true',
                        },
                        containers=[
                            ContainerArgs(
                                name='homeassistant',
                                image=f'homeassistant/home-assistant:{args.version}',
                                ports=[
                                    ContainerPortArgs(
                                        name='http',
                                        container_port=8123,
                                        protocol='TCP',
                                    ),
                                ],
                                resources=ResourceRequirementsArgs(
                                    requests={'memory': args.memory_request},
                                ),
                                volume_mounts=[
                                    VolumeMountArgs(
                                        name='hass-config',
                                        mount_path='/config',
                                    ),
                                    VolumeMountArgs(
                                        name='dbus',
                                        mount_path='/run/dbus',
                                        read_only=True,
                                    ),
                                ],
                                security_context=SecurityContextArgs(
                                    privileged=True,
                                    run_as_user=99,
                                    run_as_group=100,
                                ),
                            ),
                        ],
                        volumes=[
                            VolumeArgs(
                                name='dbus',
                                host_path=HostPathVolumeSourceArgs(
                                    path='/run/dbus',
                                ),
                            ),
                            VolumeArgs(
                                name='hass-config',
                                persistent_volume_claim=PersistentVolumeClaimVolumeSourceArgs(
                                    claim_name=self.pvc.metadata.name,
                                    read_only=False,
                                ),
                            ),
                        ],
                    ),
                ),
            ),
            opts=ResourceOptions(parent=self),
        )

        self.service = Service(
            f'{name}_service',
            metadata=ObjectMetaArgs(
                name='home-assistant',
                namespace=args.namespace,
                labels=labels,
            ),
            spec=ServiceSpecArgs(
                type='ClusterIP',
                ports=[
                    ServicePortArgs(
                        name='http',
                        port=8123,
                        target_port=8123,
                    ),
                ],
                selector=labels,
            ),
            opts=ResourceOptions(parent=self),
        )

        self.ingress = Ingress(
            f'{name}_ingress',
            metadata=ObjectMetaArgs(
                name='homeassistant',
                namespace=args.namespace,
                labels=labels,
                annotations={'pulumi.com/skipAwait': 'true'},
            ),
            spec=IngressSpecArgs(
                ingress_class_name='traefik',
                rules=[
                    IngressRuleArgs(
                        host=hostname,
                        http=HTTPIngressRuleValueArgs(
                            paths=[
                                HTTPIngressPathArgs(
                                    path='/',
                                    path_type='Prefix',
                                    backend=IngressBackendArgs(
                                        service=IngressServiceBackendArgs(
                                            name=self.service.metadata.name,
                                            port=ServiceBackendPortArgs(
                                                number=8123,
                                            ),
                                        )
                                    ),
                                ),
                            ],
                        ),
                    ),
                ],
            ),
            opts=ResourceOptions(parent=self),
        )

        self.dns = TunnelIngress(
            f'{name}_dns',
            args=TunnelIngressArgs(hostname=hostname),
            opts=ResourceOptions(parent=self),
        )

        self.register_outputs({})
