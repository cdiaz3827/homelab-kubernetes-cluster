from pulumi import Config

from .immich import ImmichArgs, ImmichDeployment

config = Config('immich')
enabled = config.get_bool('enabled')
version = config.get('version', default='v1.106.4')
hostname = config.get('hostname')


if enabled:
    db_hostname = config.require('db_hostname')
    db_port = config.require_int('db_port')
    db_username = config.require('db_username')
    db_password = config.require_secret('db_password')
    db_name = config.require('db_name')

    ImmichDeployment(
        'immich',
        args=ImmichArgs(
            version=version,
            hostname=hostname,
            db_hostname=db_hostname,
            db_port=db_port,
            db_username=db_username,
            db_password=db_password,
            db_database_name=db_name,
        ),
    )
