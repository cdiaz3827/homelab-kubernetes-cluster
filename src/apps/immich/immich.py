from pulumi import ComponentResource, ResourceOptions, export
from pulumi_kubernetes.core.v1 import (
    Namespace,
    PersistentVolumeClaim,
    PersistentVolumeClaimSpecArgs,
    VolumeResourceRequirementsArgs,
)
from pulumi_kubernetes.helm.v4 import Chart, ChartArgs, RepositoryOptsArgs
from pulumi_kubernetes.meta.v1 import ObjectMetaArgs

from modules.cloudflare import TunnelIngress, TunnelIngressArgs


class ImmichArgs:
    def __init__(
        self,
        version: str,
        hostname: str,
        db_hostname: str,
        db_port: int,
        db_username: int,
        db_password: str,
        db_database_name: str,
        namespace: str = 'immich',
        pvc_size: str = '1Gi',
        metrics_enabled: bool = False,
    ):
        self.version = version
        self.hostname = hostname
        self.namespace = namespace
        self.pvc_size = pvc_size
        self.metrics_enabled = metrics_enabled
        self.db_hostname = db_hostname
        self.db_port = db_port
        self.db_username = db_username
        self.db_password = db_password
        self.db_database_name = db_database_name


class ImmichDeployment(ComponentResource):
    def __init__(
        self, name: str, args: ImmichArgs, opts: ResourceOptions = None
    ):
        super().__init__('apps:immich', name, {}, opts)

        self.ns = Namespace(
            f'{name}_ns',
            metadata=ObjectMetaArgs(
                name=args.namespace,
            ),
            opts=ResourceOptions(parent=self),
        )

        self.pvc = PersistentVolumeClaim(
            f'{name}_pvc',
            metadata=ObjectMetaArgs(
                name='immich', namespace=self.ns.metadata.name
            ),
            spec=PersistentVolumeClaimSpecArgs(
                access_modes=['ReadWriteOnce'],
                storage_class_name='nfs-csi',
                resources=VolumeResourceRequirementsArgs(
                    requests={
                        'storage': args.pvc_size,
                    }
                ),
            ),
            opts=ResourceOptions(parent=self.ns),
        )

        self.helm = Chart(
            f'{name}_helm',
            name='immich',
            chart='immich',
            namespace=self.ns.metadata.name,
            repository_opts=RepositoryOptsArgs(
                repo='https://immich-app.github.io/immich-charts',
            ),
            values={
                'env': {
                    'DB_HOSTNAME': args.db_hostname,
                    'DB_PORT': args.db_port,
                    'DB_USERNAME': args.db_username,
                    'DB_PASSWORD': args.db_password,
                    'DB_DATABASE_NAME': args.db_database_name,
                },
                'image': {
                    'tag': args.version,
                },
                'immich': {
                    'metrics': {
                        'enabled': args.metrics_enabled,
                    },
                    'persistence': {
                        'library': {
                            'existingClaim': self.pvc.metadata.name,
                        },
                    },
                },
                'redis': {
                    'enabled': True,
                    'architecture': 'standalone',
                    'auth': {
                        'enabled': False,
                    },
                    'master': {
                        'persistence': {
                            'enabled': False,
                        }
                    },
                },
                'server': {
                    'ingress': {
                        'main': {
                            'enabled': True,
                            'annotations': {'pulumi.com/skipAwait': 'true'},
                            'hosts': [
                                {
                                    'host': args.hostname,
                                    'paths': [
                                        {
                                            'path': '/',
                                            'pathType': 'Prefix',
                                        },
                                    ],
                                }
                            ],
                        }
                    }
                },
            },
            opts=ResourceOptions(parent=self.ns),
        )

        self.dns = TunnelIngress(
            f'{name}_dns',
            args=TunnelIngressArgs(hostname=args.hostname),
            opts=ResourceOptions(parent=self),
        )
