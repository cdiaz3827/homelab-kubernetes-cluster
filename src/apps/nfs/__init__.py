import pulumi

from .nfs import Nfs, NfsArgs

config = pulumi.Config('nfs')
enabled = config.get_bool('enabled')

if enabled:
    version = config.get('nfs-csi-version', default='v4.7.0')
    server = config.require('server')
    path = config.require('path')
    nfs = Nfs(
        'nfs-csi',
        NfsArgs(
            server=server,
            path=path,
            version=version,
        ),
    )
