import pulumi
from pulumi import ComponentResource, ResourceOptions
from pulumi_kubernetes.helm.v4 import Chart, RepositoryOptsArgs
from pulumi_kubernetes.meta.v1 import ObjectMetaArgs
from pulumi_kubernetes.storage.v1 import StorageClass, StorageClassInitArgs


class NfsArgs:
    """
    Arguments for the NFS deployment.

    :param server: The NFS server hostname or IP address.
    :param path: The path to the NFS share on the server.
    :param version: The version of the NFS CSI driver chart to use. Defaults to 'v1.1.0'.
    """

    def __init__(self, server: str, path: str, version: str = 'v1.1.0'):
        self.server = server
        self.path = path
        self.version = version


class Nfs(ComponentResource):
    """
    A Pulumi component resource representing the NFS deployment.

    This component deploys the NFS CSI driver and creates a storage class
    for provisioning NFS volumes.

    :param name: The name of the component resource.
    :param args: The arguments for the NFS deployment.
    :param opts: Optional resource options.
    """

    def __init__(self, name: str, args: NfsArgs, opts: ResourceOptions = None):
        super().__init__('core:nfs-csi', name, {}, opts)

        self.nfs_driver = Chart(
            f'{name}_release',
            name='csi-driver-nfs',
            chart='csi-driver-nfs',
            version=args.version,
            namespace='kube-system',
            repository_opts=RepositoryOptsArgs(
                repo='https://raw.githubusercontent.com/kubernetes-csi/csi-driver-nfs/master/charts'
            ),
            opts=ResourceOptions(parent=self),
        )
        self.nfs_storage_class = StorageClass(
            f'{name}_storage_class',
            args=StorageClassInitArgs(
                metadata=ObjectMetaArgs(name='nfs-csi'),
                provisioner='nfs.csi.k8s.io',
                parameters={
                    'server': args.server,
                    'share': args.path,
                },
                reclaim_policy='Delete',
                volume_binding_mode='Immediate',
                mount_options=['nfsvers=4.1'],
            ),
            opts=ResourceOptions(parent=self.nfs_driver),
        )
        self.register_outputs({})
