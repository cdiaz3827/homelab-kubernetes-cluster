import pulumi_kubernetes.core.v1 as core
import pulumi_kubernetes.helm.v4 as helm
import pulumi_kubernetes.meta.v1 as meta
from pulumi import ComponentResource, ResourceOptions, export

from modules.cloudflare import TunnelIngress, TunnelIngressArgs


class OnePasswordArgs:
    def __init__(
        self,
        version: str,
        enable_connect: bool = True,
        connect_credentials: str = None,
        connect_ingress: bool = True,
        connect_hostname: str = 'op.cdiaz.cloud',
        enable_operator: bool = True,
        operator_token: str = None,
        operator_auto_restart: bool = True,
    ):
        self.version = version
        self.enable_connect = enable_connect
        self.connect_credentials = connect_credentials
        self.connect_ingress = connect_ingress
        self.connect_hostname = connect_hostname
        self.enable_operator = enable_operator
        self.operator_token = operator_token
        self.operator_auto_restart = operator_auto_restart


class OnePassword(ComponentResource):
    def __init__(
        self, name: str, args: OnePasswordArgs, opts: ResourceOptions = None
    ):
        super().__init__('apps:onepassword', name, {}, opts)

        self.ns = core.Namespace(
            'onepassword_ns',
            metadata=meta.ObjectMetaArgs(
                name='onepassword',
            ),
            opts=ResourceOptions(parent=self),
        )

        self.chart = helm.Chart(
            f'{name}_chart',
            name='onepassword',
            chart='connect',
            namespace=self.ns.metadata.name,
            version=args.version,
            repository_opts=helm.RepositoryOptsArgs(
                repo='https://1password.github.io/connect-helm-charts'
            ),
            values={
                'connect': {
                    'create': args.enable_connect,
                    'credentials': args.connect_credentials,
                    'serviceType': 'ClusterIP',
                    'ingress': {
                        'enabled': args.connect_ingress,
                        'hosts': [
                            {
                                'host': args.connect_hostname,
                                'paths': ['/'],
                            }
                        ],
                        'ingressClassName': 'traefik',
                        'annotations': {'pulumi.com/skipAwait': 'true'},
                    },
                },
                'operator': {
                    'create': args.enable_operator,
                    'autoRestart': args.operator_auto_restart,
                    'token': {'value': args.operator_token},
                },
            },
            opts=ResourceOptions(parent=self.ns, delete_before_replace=True),
        )

        self.dns = TunnelIngress(
            f'{name}_dns',
            TunnelIngressArgs(
                hostname=args.connect_hostname,
            ),
            opts=ResourceOptions(parent=self, delete_before_replace=True),
        )

        export(f'{name}_dns', self.dns.record.hostname)

        self.register_outputs({})
