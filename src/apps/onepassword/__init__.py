import base64

import pulumi_kubernetes as k8s
from pulumi import Config, Output, ResourceOptions

from .onepassword import OnePassword, OnePasswordArgs

config = Config('onepassword')
enabled = config.get_bool('enabled', default=False)
namespace = config.get('namespace', default='onepassword')
version = config.get('version', default='1.15.0')
hostname = config.get('hostname', default='op.cdiaz.cloud')

op_credentials = config.get_secret('credentials')
op_token = config.get_secret('token')

if enabled:
    op = OnePassword(
        'onepassword',
        args=OnePasswordArgs(
            connect_hostname=hostname,
            connect_ingress=True,
            version=version,
            operator_token=op_token,
            connect_credentials=op_credentials,
        ),
    )
