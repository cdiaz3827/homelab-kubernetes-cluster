import pulumi_kubernetes as kubernetes
from pulumi import ComponentResource, Config, Output, ResourceOptions, export
from pulumi_cloudflare import Record

from modules.db import PostgresArgs, PostgresDatabase
from modules.dns import zone
from networking.cloudflared import tunnel_domain

db_config = Config('postgresql')
db_host = db_config.get('host')


class MealieArgs:
    """
    Arguments for the Mealie application deployment.

    :param hostname: The hostname for the Mealie application.
    :param version: The version of the Mealie application to deploy. Defaults to 'v.1.6.0'.
    :param allow_signup: Whether to allow user signup. Defaults to False.
    :param log_level: The logging level for the Mealie application. Defaults to 'warning'.
    :param timezone: The timezone for the Mealie application. Defaults to 'America/Phoenix'.
    :param smtp_host: The SMTP server hostname.
    :param smtp_port: The SMTP server port. Defaults to 587.
    :param smtp_from_name: The name to use for sending emails. Defaults to 'Mealie'.
    :param smtp_from_email: The email address to use for sending emails.
    :param smtp_user: The SMTP server username.
    :param smtp_password: The SMTP server password.
    :param openai_base_url: The base URL for the OpenAI API.
    :param openai_api_key: The OpenAI API key.
    :param openai_model: The OpenAI model to use. Defaults to 'gpt-4o'.
    :param openai_workers: The number of OpenAI workers to use. Defaults to 2.
    :param openai_send_database_data: Whether to send database data to OpenAI. Defaults to True.
    :param oidc_auth_enabled: Whether to enable OpenID Connect (OIDC) authentication. Defaults to False.
    :param oidc_signup_enabled: Whether to enable user signup using OIDC. Defaults to True.
    :param oidc_configuration_url: The URL for the OIDC configuration.
    :param oidc_client_id: The OIDC client ID.
    :param oidc_user_group: The group to use for regular users.
    :param oidc_admin_group: The group to use for administrators.
    :param oidc_auto_redirect: Whether to automatically redirect users to the OIDC provider. Defaults to False.
    :param oidc_provider_name: The name of the OIDC provider. Defaults to 'OAuth'.
    :param oidc_remember_me: Whether to enable the "remember me" feature for OIDC. Defaults to False.
    :param oidc_signing_algorithm: The signing algorithm to use for OIDC. Defaults to 'RS256'.
    :param oidc_user_claim: The claim to use for the user's username. Defaults to 'email'.
    :param oidc_groups_claim: The claim to use for the user's groups. Defaults to 'groups'.
    :param oidc_tls_cacertfile: The path to the TLS certificate file for the OIDC provider.
    """

    def __init__(
        self,
        # General Config
        hostname: str,
        version: str = 'v.1.6.0',
        allow_signup: bool = False,
        log_level: str = 'warning',
        timezone: str = 'America/Phoenix',
        # SMTP
        smtp_host: str = None,
        smtp_port: int = 587,
        smtp_from_name: str = 'Mealie',
        smtp_from_email: str = None,
        smtp_user: str = None,
        smtp_password: str = None,
        # OpenAI
        openai_base_url: str = None,
        openai_api_key: str = None,
        openai_model: str = 'gpt-4o',
        openai_workers: int = 2,
        openai_send_database_data: bool = True,
        # OIDC SSO
        oidc_auth_enabled: bool = False,
        oidc_signup_enabled: bool = True,
        oidc_configuration_url: str = None,
        oidc_client_id: str = None,
        oidc_user_group: str = None,
        oidc_admin_group: str = None,
        oidc_auto_redirect: bool = False,
        oidc_provider_name: str = 'OAuth',
        oidc_remember_me: bool = False,
        oidc_signing_algorithm: str = 'RS256',
        oidc_user_claim: str = 'email',
        oidc_groups_claim: str = 'groups',
        oidc_tls_cacertfile: str = None,
    ):
        # General Config
        self.hostname = hostname
        self.version = version
        self.allow_signup = allow_signup
        self.log_level = log_level
        self.timezone = timezone
        # SMTP
        self.smtp_host = smtp_host
        self.smtp_port = smtp_port
        self.smtp_from_name = smtp_from_name
        self.smtp_from_email = smtp_from_email
        self.smtp_user = smtp_user
        self.smtp_password = smtp_password
        # OpenAPI
        self.openai_base_url = openai_base_url
        self.openai_api_key = openai_api_key
        self.openai_model = openai_model
        self.openai_workers = openai_workers
        self.openai_send_database_data = openai_send_database_data
        # OIDC SSO
        self.oidc_auth_enabled = oidc_auth_enabled
        self.oidc_signup_enabled = oidc_signup_enabled
        self.oidc_configuration_url = oidc_configuration_url
        self.oidc_client_id = oidc_client_id
        self.oidc_user_group = oidc_user_group
        self.oidc_admin_group = oidc_admin_group
        self.oidc_auto_redirect = oidc_auto_redirect
        self.oidc_provider_name = oidc_provider_name
        self.oidc_remember_me = oidc_remember_me
        self.oidc_signing_algorithm = oidc_signing_algorithm
        self.oidc_user_claim = oidc_user_claim
        self.oidc_groups_claim = oidc_groups_claim
        self.oidc_tls_cacertfile = oidc_tls_cacertfile


class Mealie(ComponentResource):
    """
    A Pulumi component resource representing the Mealie application deployment.

    This component deploys the Mealie application to a Kubernetes cluster,
    configuring it with the provided settings.

    :param name: The name of the component resource.
    :param args: The arguments for the Mealie application deployment.
    :param opts: Optional resource options.
    """

    def __init__(
        self, name: str, args: MealieArgs, opts: ResourceOptions = None
    ):
        super().__init__('apps:mealie', name, {}, opts)

        self.db = PostgresDatabase(
            f'{name}_database',
            PostgresArgs(db_name=name, db_username=f'{name}user'),
            opts=ResourceOptions(parent=self),
        )

        self.namespace = kubernetes.core.v1.Namespace(
            f'{name}_namespace',
            metadata=kubernetes.meta.v1.ObjectMetaArgs(name='mealie'),
            opts=ResourceOptions(parent=self),
        )
        child_opts = ResourceOptions(parent=self.namespace)
        common_labels = {
            'app': 'mealie',
            'component': 'mealie',
        }
        common_metadata = kubernetes.meta.v1.ObjectMetaArgs(
            name='mealie',
            namespace=self.namespace.metadata.name,
            labels=common_labels,
        )
        self.config_map = kubernetes.core.v1.ConfigMap(
            f'{name}_config_map',
            metadata=common_metadata,
            data={
                'ALLOW_SIGNUP': str(args.allow_signup),
                'BASE_URL': f'https://{args.hostname}',
                'PUID': '99',
                'PGID': '100',
                'TZ': args.timezone,
                'LOG_LEVEL': args.log_level,
            },
            opts=child_opts,
        )
        self.secrets = kubernetes.core.v1.Secret(
            f'{name}-secret',
            metadata=common_metadata,
            string_data={
                # Database
                'DB_ENGINE': 'postgres',
                'POSTGRES_USER': self.db.user.name,
                'POSTGRES_PASSWORD': self.db.user.password,
                'POSTGRES_SERVER': db_host,
                'POSTGRES_PORT': str(5432),
                'POSTGRES_DB': self.db.database.name,
                # SMTP
                'SMTP_HOST': args.smtp_host,
                'SMTP_PORT': args.smtp_port,
                'SMTP_AUTH_STRATEGY': 'TLS',
                'SMTP_FROM_NAME': args.smtp_from_name,
                'SMTP_FROM_EMAIL': args.smtp_from_email,
                'SMTP_USER': args.smtp_user,
                'SMTP_PASSWORD': args.smtp_password,
                # OpenAI
                'OPENAI_BASE_URL': args.openai_base_url,
                'OPENAI_API_KEY': args.openai_api_key,
                'OPENAI_MODEL': args.openai_model,
                'OPENAI_WORKERS': str(args.openai_workers),
                'OPENAI_SEND_DATABASE_DATA': str(
                    args.openai_send_database_data
                ),
                # OIDC SSO
                'OIDC_AUTH_ENABLED': str(args.oidc_auth_enabled),
                'OIDC_SIGNUP_ENABLED': str(args.oidc_signup_enabled),
                'OIDC_CONFIGURATION_URL': args.oidc_configuration_url,
                'OIDC_CLIENT_ID': args.oidc_client_id,
                'OIDC_USER_GROUP': args.oidc_user_group,
                'OIDC_ADMIN_GROUP': args.oidc_admin_group,
                'OIDC_AUTO_REDIRECT': str(args.oidc_auto_redirect),
                'OIDC_PROVIDER_NAME': args.oidc_provider_name,
                'OIDC_REMEMBER_ME': str(args.oidc_remember_me),
                'OIDC_SIGNING_ALGORITHM': args.oidc_signing_algorithm,
                'OIDC_USER_CLAIM': args.oidc_user_claim,
                'OIDC_GROUPS_CLAIM': args.oidc_groups_claim,
                'OIDC_TLS_CACERTFILE': args.oidc_tls_cacertfile,
            },
            opts=child_opts,
        )
        self.pvc = kubernetes.core.v1.PersistentVolumeClaim(
            f'{name}_pvc',
            metadata=common_metadata,
            spec=kubernetes.core.v1.PersistentVolumeClaimSpecArgs(
                access_modes=['ReadWriteOnce'],
                storage_class_name='nfs-csi',
                resources=kubernetes.core.v1.VolumeResourceRequirementsArgs(
                    requests={'storage': '50Mi'}
                ),
            ),
            opts=child_opts,
        )
        self.deployment = kubernetes.apps.v1.Deployment(
            f'{name}_deployment',
            metadata=common_metadata,
            spec=kubernetes.apps.v1.DeploymentSpecArgs(
                replicas=1,
                selector=kubernetes.meta.v1.LabelSelectorArgs(
                    match_labels=common_labels,
                ),
                template=kubernetes.core.v1.PodTemplateSpecArgs(
                    metadata=common_metadata,
                    spec=kubernetes.core.v1.PodSpecArgs(
                        containers=[
                            kubernetes.core.v1.ContainerArgs(
                                name='mealie',
                                image=f'ghcr.io/mealie-recipes/mealie:{args.version}',
                                ports=[
                                    kubernetes.core.v1.ContainerPortArgs(
                                        name='api',
                                        container_port=9000,
                                    )
                                ],
                                volume_mounts=[
                                    kubernetes.core.v1.VolumeMountArgs(
                                        name='mealie',
                                        mount_path='/app/data',
                                    )
                                ],
                                env_from=[
                                    kubernetes.core.v1.EnvFromSourceArgs(
                                        config_map_ref=kubernetes.core.v1.ConfigMapEnvSourceArgs(
                                            name=self.config_map.metadata.name,
                                            optional=False,
                                        )
                                    ),
                                    kubernetes.core.v1.EnvFromSourceArgs(
                                        secret_ref=kubernetes.core.v1.SecretEnvSourceArgs(
                                            name=self.secrets.metadata.name,
                                            optional=False,
                                        )
                                    ),
                                ],
                            )
                        ],
                        volumes=[
                            kubernetes.core.v1.VolumeArgs(
                                name='mealie',
                                persistent_volume_claim=kubernetes.core.v1.PersistentVolumeClaimVolumeSourceArgs(
                                    claim_name=self.pvc.metadata.name
                                ),
                            )
                        ],
                    ),
                ),
            ),
            opts=child_opts,
        )
        self.service = kubernetes.core.v1.Service(
            f'{name}_service',
            metadata=common_metadata,
            spec=kubernetes.core.v1.ServiceSpecArgs(
                selector=common_labels,
                type='ClusterIP',
                ports=[
                    kubernetes.core.v1.ServicePortArgs(
                        port=9000, target_port='api', protocol='TCP'
                    )
                ],
            ),
            opts=child_opts,
        )
        common_metadata.annotations = {'pulumi.com/skipAwait': 'true'}
        self.ingress = kubernetes.networking.v1.Ingress(
            f'{name}_ingress',
            metadata=common_metadata,
            spec=kubernetes.networking.v1.IngressSpecArgs(
                ingress_class_name='traefik',
                rules=[
                    kubernetes.networking.v1.IngressRuleArgs(
                        host=args.hostname,
                        http=kubernetes.networking.v1.HTTPIngressRuleValueArgs(
                            paths=[
                                kubernetes.networking.v1.HTTPIngressPathArgs(
                                    path='/',
                                    path_type='Prefix',
                                    backend=kubernetes.networking.v1.IngressBackendArgs(
                                        service=kubernetes.networking.v1.IngressServiceBackendArgs(
                                            name=self.service.metadata.name,
                                            port=kubernetes.networking.v1.ServiceBackendPortArgs(
                                                number=9000,
                                            ),
                                        )
                                    ),
                                ),
                            ],
                        ),
                    ),
                ],
            ),
            opts=child_opts,
        )
        self.dns = Record(
            f'{name}_dns_record',
            name=args.hostname,
            type='CNAME',
            value=tunnel_domain,
            zone_id=zone.zone_id,
            allow_overwrite=True,
            proxied=True,
            opts=ResourceOptions(parent=self),
        )
        export('Mealie URL', Output.format('https://{0}', self.dns.hostname))
        self.register_outputs({})
