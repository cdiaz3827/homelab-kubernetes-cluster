"""
This module defines the Mealie application deployment using Pulumi.

It configures the Mealie application with settings for hostname, version, logging level, SMTP, OpenAI, and SSO.

**Configuration:**

- `enabled`: Boolean value indicating whether to deploy the Mealie application.
- `hostname`: The hostname for the Mealie application.
- `version`: The version of the Mealie application to deploy. Defaults to 'latest'.
- `smtp.host`: The SMTP server hostname.
- `smtp.port`: The SMTP server port.
- `smtp.username`: The SMTP server username.
- `smtp.token`: The SMTP server password.
- `smtp.from_email`: The email address to use for sending emails.
- `openai.apiKey`: The OpenAI API key.
- `oidc_configuration_url`: The URL for the OpenID Connect (OIDC) configuration.
- `oidc_client_id`: The OIDC client ID.
- `oidc_user_claim`: The claim to use for the user's username.
- `oidc_admin_group`: The group to use for administrators.
- `oidc_user_group`: The group to use for regular users.

**Example Usage:**

```python
import pulumi

# Configure Mealie application
pulumi.Config('mealie').get('enabled', True)
pulumi.Config('mealie').get('hostname', 'mealie.example.com')
pulumi.Config('mealie').get('version', 'latest')

# Configure SMTP settings
pulumi.Config('smtp').get('host', 'smtp.example.com')
pulumi.Config('smtp').get('port', 587)
pulumi.Config('smtp').get('username', 'user@example.com')
pulumi.Config('smtp').get_secret('token', 'your_smtp_password')
pulumi.Config('smtp').get('from_email', 'user@example.com')

# Configure OpenAI settings
pulumi.Config('openai').get_secret('apiKey', 'your_openai_api_key')

# Configure SSO settings
pulumi.Config('mealie').get_secret('oidc_configuration_url', 'https://your_oidc_provider/config')
pulumi.Config('mealie').get_secret('oidc_client_id', 'your_oidc_client_id')
pulumi.Config('mealie').get('oidc_user_claim', 'email')
pulumi.Config('mealie').get('oidc_admin_group', 'admins')
pulumi.Config('mealie').get('oidc_user_group', 'users')
"""
import pulumi

from .mealie import Mealie, MealieArgs

# Setup Config
config = pulumi.Config('mealie')
enabled = config.get_bool('enabled')
smtp = pulumi.Config('smtp')
openai = pulumi.Config('openai')
version = config.get('version', default='latest')
log_level = config.get('log_level', default='warning')

# Setup Namespace
if enabled:
    mealie = Mealie(
        'mealie',
        MealieArgs(
            hostname=config.require('hostname'),
            version=version,
            log_level=log_level,
            # SMTP
            smtp_host=smtp.get('host'),
            smtp_port=smtp.get('port'),
            smtp_user=smtp.get('username'),
            smtp_password=smtp.get_secret('token'),
            smtp_from_email=smtp.get('username'),
            # OpenAI
            openai_api_key=openai.get_secret('apiKey'),
            openai_workers=1,
            # SSO
            oidc_auth_enabled=True,
            oidc_signup_enabled=True,
            oidc_auto_redirect=True,
            oidc_configuration_url=config.get_secret('oidc_configuration_url'),
            oidc_client_id=config.get_secret('oidc_client_id'),
            oidc_provider_name='Authentik',
            oidc_user_claim=config.get('oidc_user_claim'),
            oidc_admin_group=config.get('oidc_admin_group'),
            oidc_user_group=config.get('oidc_user_group'),
        ),
    )
