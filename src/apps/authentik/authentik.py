from pulumi import ComponentResource, Config, Output, ResourceOptions, export
from pulumi_cloudflare import Record
from pulumi_kubernetes.helm.v3 import Release, ReleaseArgs, RepositoryOptsArgs
from pulumi_random import RandomBytes

from modules.db import PostgresArgs, PostgresDatabase
from modules.dns import zone
from networking.cloudflared import tunnel_domain

db_config = Config('postgresql')
db_host = db_config.get('host')


class AuthentikArgs:
    """
    Arguments for the Authentik Component Resource.

    :param hostname: The hostname for the Authentik instance.
    :param version: The version of the Authentik Helm chart to use. Defaults to '2024.4.2'.
    :param error_reporting: Whether to enable error reporting. Defaults to False.
    :param log_level: The log level for the Authentik instance. Defaults to 'warning'.
    :param postgresql_host: The hostname of the PostgreSQL database. Defaults to ''.
    :param postgresql_name: The name of the PostgreSQL database. Defaults to 'authentik'.
    :param postgresql_password: The password for the PostgreSQL database. Defaults to ''.
    :param postgresql_port: The port for the PostgreSQL database. Defaults to 5432.
    :param postgresql_user: The username for the PostgreSQL database. Defaults to 'authentik'.
    :param email_from: The email address to use for sending emails. Defaults to ''.
    :param email_host: The hostname of the SMTP server. Defaults to ''.
    :param email_password: The password for the SMTP server. Defaults to ''.
    :param email_port: The port for the SMTP server. Defaults to 587.
    :param email_timeout: The timeout for the SMTP connection. Defaults to 30.
    :param email_use_ssl: Whether to use SSL for the SMTP connection. Defaults to False.
    :param email_use_tls: Whether to use TLS for the SMTP connection. Defaults to False.
    :param email_username: The username for the SMTP server. Defaults to ''.
    """

    def __init__(
        self,
        hostname: str,
        version: str = '2024.4.2',
        error_reporting: bool = False,
        log_level: str = 'warning',
        email_from: str = '',
        email_host: str = '',
        email_password: str = '',
        email_port: int = 587,
        email_timeout: int = 30,
        email_use_ssl: bool = False,
        email_use_tls: bool = False,
        email_username: str = '',
    ):
        self.hostname = hostname
        self.version = version
        self.error_reporting = error_reporting
        self.log_level = log_level
        self.email_from = email_from
        self.email_host = email_host
        self.email_password = email_password
        self.email_port = email_port
        self.email_timeout = email_timeout
        self.email_use_ssl = email_use_ssl
        self.email_use_tls = email_use_tls
        self.email_username = email_username


class Authentik(ComponentResource):
    """
    A component resource for deploying Authentik.

    :param name: The name of the component resource.
    :param args: The arguments for the Authentik component.
    :param opts: Optional resource options.
    """

    def __init__(
        self, name: str, args: AuthentikArgs, opts: ResourceOptions = None
    ):
        super().__init__('apps:authentik', name, {}, opts)

        child_opts = ResourceOptions(parent=self)

        secret_key = RandomBytes(
            f'{name}_secret_key', length=60, opts=child_opts
        )

        db = PostgresDatabase(
            f'{name}_db',
            PostgresArgs(db_name='authentik', db_username='authentikdbuser'),
            opts=ResourceOptions(parent=self),
        )

        self.authentik = Release(
            f'{name}_authentik_release',
            ReleaseArgs(
                name='authentik',
                chart='authentik',
                version=args.version,
                namespace='authentik',
                create_namespace=True,
                repository_opts=RepositoryOptsArgs(
                    repo='https://charts.goauthentik.io',
                ),
                values={
                    'global': {
                        'storageClass': 'nfs-csi',
                        'clusterDomain': 'homelab.cdiaz.cloud',
                    },
                    'authentik': {
                        'secret_key': secret_key.base64,
                        'error_reporting': {
                            'enabled': args.error_reporting,
                        },
                        'log_level': args.log_level,
                        'email': {
                            'from': args.email_from,
                            'host': args.email_host,
                            'password': args.email_password,
                            'port': args.email_port,
                            'timeout': args.email_timeout,
                            'use_ssl': args.email_use_ssl,
                            'use_tls': args.email_use_tls,
                            'username': args.email_username,
                        },
                        'postgresql': {
                            'host': db_host,
                            'port': 5432,
                            'name': db.database.name,
                            'user': db.user.name,
                            'password': db.user.password,
                        },
                    },
                    'redis': {
                        'enabled': True,
                        'architecture': 'standalone',
                        'master': {'persistence': {'enabled': False}},
                    },
                    'server': {
                        'ingress': {
                            'enabled': True,
                            'ingressClassName': 'traefik',
                            'hosts': [args.hostname],
                        }
                    },
                },
            ),
            opts=child_opts,
        )
        self.dns = Record(
            f'{name}_dns_record',
            name=args.hostname,
            type='CNAME',
            value=tunnel_domain,
            zone_id=zone.zone_id,
            allow_overwrite=True,
            proxied=True,
            opts=child_opts,
        )
        export(
            'Authentik URL', Output.format('https://{0}', self.dns.hostname)
        )
        self.register_outputs({})
