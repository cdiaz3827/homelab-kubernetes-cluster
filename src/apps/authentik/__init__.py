import pulumi

from .authentik import Authentik, AuthentikArgs

config = pulumi.Config('authentik')
enabled = config.get_bool('enabled')
hostname = config.get('hostname')

if enabled:
    authentik = Authentik(
        'authentik',
        AuthentikArgs(
            hostname=hostname,
        ),
    )
