from pulumi import Config

from modules.namespace import K8sNamespace

from .grafana import Grafana, GrafanaArgs

config = Config('observability')

grafana_config = Config('grafana')
grafana_enabled = grafana_config.get_bool('enabled', default=False)


namespace = K8sNamespace(name='observability')

if grafana_enabled:
    grafana_hostname = grafana_config.require('hostname')
    grafana_user = grafana_config.get('username', 'admin')
    grafana_password = grafana_config.get_secret('password')

    Grafana(
        'grafana',
        GrafanaArgs(
            namespace=namespace.metadata.name,
            hostname=grafana_hostname,
            admin_username=grafana_user,
            admin_password=grafana_password,
        ),
    )
