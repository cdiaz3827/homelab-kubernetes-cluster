from pulumi import ComponentResource, ResourceOptions
from pulumi_kubernetes.helm.v4 import Chart, RepositoryOptsArgs

from modules.cloudflare import TunnelIngress, TunnelIngressArgs
from modules.ingress import K8sIngress


class GrafanaArgs:
    def __init__(
        self,
        namespace: str,
        hostname: str,
        admin_username: str,
        admin_password: str,
    ):
        self.namespace = namespace
        self.hostname = hostname
        self.admin_username = admin_username
        self.admin_password = admin_password


class Grafana(ComponentResource):
    def __init__(
        self, name: str, args: GrafanaArgs, opts: ResourceOptions = None
    ):
        super().__init__('observability:Grafana', name, {}, opts)

        child_opts = ResourceOptions(parent=self)

        self.helm = Chart(
            f'{name}_helm',
            name='grafana',
            namespace=args.namespace,
            chart='grafana',
            repository_opts=RepositoryOptsArgs(
                repo='https://grafana.github.io/helm-charts'
            ),
            values={
                'securityContext': {
                    'runAsUser': 99,
                    'runAsGroup': 100,
                    'fsGroup': 100,
                },
                'ingress': {
                    'enabled': True,
                    'hosts': [args.hostname],
                    'annotations': {'pulumi.com/skipAwait': 'true'},
                },
                'persistence': {
                    'type': 'pvc',
                    'enabled': True,
                    'storageClassName': 'nfs-csi',
                    'size': '1Gi',
                },
                'adminUser': args.admin_username,
                'adminPassword': args.admin_password,
            },
            opts=child_opts,
        )

        self.dns = TunnelIngress(
            f'{name}_dns', args=TunnelIngressArgs(hostname=args.hostname)
        )
