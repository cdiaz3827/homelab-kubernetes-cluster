from pulumi import ComponentResource, ResourceOptions
from pulumi_postgresql import Database, Role
from pulumi_random import RandomPassword


class PostgresArgs:
    """
    Arguments for creating a Postgres database.

    :param db_name: The name of the database.
    :param db_username: The username for the database.
    :param db_password: The password for the database. If not provided, a random password will be generated.
    """

    def __init__(
        self,
        db_name: str,
        db_username: str,
        db_password: str = None,
    ):
        self.db_name = db_name
        self.db_username = db_username
        self.db_password = db_password


class PostgresDatabase(ComponentResource):
    """
    A Pulumi component resource that creates a Postgres database and user.

    :param name: The name of the component resource.
    :param args: The arguments for creating the database and user.
    :param opts: Optional resource options.
    """

    def __init__(
        self, name: str, args: PostgresArgs, opts: ResourceOptions = None
    ):
        super().__init__('db:postgres', name, {}, opts)

        child_opts = ResourceOptions(parent=self)

        if not args.db_password:
            password = RandomPassword(
                f'{args.db_username}_password', length=32, opts=child_opts
            )
            args.db_password = password.result

        self.database = Database(
            f'{args.db_name}_database',
            name=args.db_name,
            opts=ResourceOptions(parent=self, delete_before_replace=True),
        )

        self.user = Role(
            f'{args.db_username}_db_user',
            name=args.db_username,
            password=args.db_password,
            login=True,
            superuser=True,
            opts=ResourceOptions(parent=self, delete_before_replace=True),
        )

        self.database_name = self.database.name
        self.username = self.user.name
        self.password = self.user.password

        self.register_outputs(
            {
                'name': self.database_name,
                'username': self.username,
                'password': self.password,
            }
        )
