from pulumi import Config, Output, export, get_project, get_stack
from pulumi_cloudflare import Record, get_zone

from networking.cloudflared import tunnel_domain

# Setup Vars
config = Config('homelab')
domain = config.require('primary-domain')
project = get_project()
stack = get_stack()

# Get Cloudflare Zone
zone = get_zone(name=domain)


## DNS Record Function
def cname_record(
    name: str,
    cname_value: str,
):
    """
    Creates a CNAME record within a Cloudflare zone.

    Args:
        name: The name of the DNS record (e.g., 'www').
        cname_value: The CNAME value (e.g., 'my-app.example.com').

    Returns:
        The created Cloudflare Record object.
    """
    record = Record(
        f'{name}-dns-record',
        name=name,
        type='CNAME',
        value=cname_value,
        zone_id=zone.id,
        allow_overwrite=True,
        proxied=True,
        comment=f'Managed by Pulumi: {project}/{stack}',
    )
    export(name, Output.format('https://{0}', record.hostname))
    return record
