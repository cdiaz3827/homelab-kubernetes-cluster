import pulumi_kubernetes.helm.v4 as helm
from pulumi import ComponentResource, Output, ResourceOptions
from pulumi_random import RandomPassword


class RedisArgs:
    """
    Arguments for the Redis deployment.

    :param namespace: The Kubernetes namespace to deploy Redis to.
    :param version: The version of the Redis chart to use. Defaults to 'v10.1.0'.
    """

    def __init__(
        self,
        namespace: str,
        auth_enabled: bool = True,
        redis_pw: str = None,
    ):
        self.namespace = namespace
        self.auth_enabled = auth_enabled
        self.redis_pw = redis_pw


class Redis(ComponentResource):
    """
    A Pulumi component resource representing the Redis deployment.

    This component deploys the Redis chart from Bitnami.

    :param name: The name of the component resource.
    :param args: The arguments for the Redis deployment.
    :param opts: Optional resource options.
    """

    def __init__(
        self, name: str, args: RedisArgs, opts: ResourceOptions = None
    ):
        super().__init__('cache:redis', name, {}, opts)

        if not args.redis_pw:
            password = RandomPassword(
                f'{name}_redis_password',
                length=32,
                special=False,
                opts=ResourceOptions(parent=self),
            )
            args.redis_pw = password.result

        self.chart = helm.Chart(
            f'{name}_redis_chart',
            name='redis',
            chart='redis',
            namespace=args.namespace,
            repository_opts=helm.RepositoryOptsArgs(
                repo='https://charts.bitnami.com/bitnami'
            ),
            values={
                'architecture': 'standalone',
                'auth': {
                    'enabled': args.auth_enabled,
                    'password': args.redis_pw,
                },
                'master': {
                    'persistence': {
                        'enabled': False,
                    },
                },
            },
            opts=ResourceOptions(parent=self),
        )

        # Build Output Strings
        self.password = args.redis_pw
        self.namespace = args.namespace
        self.service_address = Output.concat(
            'redis-master', '.', args.namespace
        )
        self.port = 6379
        self.connection_string = Output.concat(
            'redis://',
            ':',
            self.password,
            '@',
            self.service_address,
            ':',
            str(self.port),
        )

        # Register Outputs
        self.register_outputs(
            {
                'password': self.password,
                'namespace': self.namespace,
                'connection_string': self.connection_string,
                'service_address': self.service_address,
                'port': str(self.port),
            }
        )
