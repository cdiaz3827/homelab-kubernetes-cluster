from pulumi import (
    ComponentResource,
    Config,
    Output,
    ResourceOptions,
    export,
    get_project,
    get_stack,
)
from pulumi_cloudflare import Record, get_zone

from networking.cloudflared import tunnel_domain

# Setup Vars
config = Config('homelab')
domain = config.require('primary-domain')
project = get_project()
stack = get_stack()

# Get Cloudflare Zone
zone = get_zone(name=domain)


class TunnelIngressArgs:
    def __init__(
        self,
        hostname: str,
        cname_value: str = tunnel_domain,
    ):
        self.hostname = hostname
        self.cname_value = cname_value


class TunnelIngress(ComponentResource):
    def __init__(
        self, name: str, args: TunnelIngressArgs, opts: ResourceOptions = None
    ):
        super().__init__('dns:tunnel-ingress', name, {}, opts)

        self.record = Record(
            f'{name}_record',
            name=args.hostname,
            type='CNAME',
            value=args.cname_value,
            zone_id=zone.id,
            allow_overwrite=True,
            proxied=True,
            comment=f'Managed by Pulumi: {project}/{stack}',
            opts=ResourceOptions(parent=self, delete_before_replace=True),
        )

        # Build Output
        name = name.replace('_dns', '')
        export(
            f'{name.capitalize()} URL',
            Output.format('https://{0}', self.record.hostname),
        )
        self.register_outputs({})
