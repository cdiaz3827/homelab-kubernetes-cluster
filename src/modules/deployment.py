from collections.abc import Sequence

import pulumi_kubernetes.apps.v1 as apps
import pulumi_kubernetes.core.v1 as core
import pulumi_kubernetes.meta.v1 as meta
from pulumi import ResourceOptions


def SimpleDeployment(
    name: str,
    namespace: str,
    image: str,
    component: str = 'Web',
    run_as_user: int = 99,
    run_as_group: int = 100,
    fs_group: int = 100,
    version_tag: str = 'latest',
    default_port: int = 80,
    default_port_name: str = 'http',
    readiness_probe: core.ProbeArgs = None,
    liveness_probe: core.ProbeArgs = None,
    startup_probe: core.ProbeArgs = None,
    resources: core.ResourceRequirementsArgs = None,
    volume_mounts: Sequence[core.VolumeMountArgs] = None,
    volumes: Sequence[core.VolumeArgs] = None,
    opts: ResourceOptions = None,
):

    labels = {
        'app': name,
        'component': component,
        'version': version_tag,
    }
    deployment = apps.Deployment(
        f'{name}_deployment',
        metadata=meta.ObjectMetaArgs(
            name=name,
            namespace=namespace,
            labels=labels,
        ),
        spec=apps.DeploymentSpecArgs(
            selector=meta.LabelSelectorArgs(match_labels=labels),
            template=core.PodTemplateSpecArgs(
                metadata=meta.ObjectMetaArgs(labels=labels),
                spec=core.PodSpecArgs(
                    security_context=core.PodSecurityContextArgs(
                        run_as_user=run_as_user,
                        run_as_group=run_as_group,
                        fs_group=fs_group,
                    ),
                    containers=[
                        core.ContainerArgs(
                            name=name,
                            image=f'{image}:{version_tag}',
                            ports=[
                                core.ContainerPortArgs(
                                    name=default_port_name,
                                    container_port=default_port,
                                    protocol='TCP',
                                ),
                            ],
                            readiness_probe=readiness_probe,
                            liveness_probe=liveness_probe,
                            startup_probe=startup_probe,
                            resources=resources,
                            volume_mounts=volume_mounts,
                        ),
                    ],
                    volumes=volumes,
                ),
            ),
        ),
        opts=opts,
    )
    return deployment
