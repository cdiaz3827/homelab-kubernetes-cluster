from pulumi import ResourceOptions
from pulumi_kubernetes.core.v1 import (
    CSIPersistentVolumeSourceArgs,
    PersistentVolume,
    PersistentVolumeInitArgs,
    PersistentVolumeSpecArgs,
)
from pulumi_kubernetes.meta.v1 import ObjectMetaArgs


def nfs_pv(
    name: str,
    nfs_server: str,
    nfs_path: str,
    storageCapacity: str = '1Gi',
    opts: ResourceOptions = None,
):
    pv = PersistentVolume(
        f'{name}_persistent_volume',
        args=PersistentVolumeInitArgs(
            metadata=ObjectMetaArgs(
                name=name,
                annotations={
                    'pv.kubernetes.io/provisioned-by': 'nfs.csi.k8s.io',
                },
            ),
            spec=PersistentVolumeSpecArgs(
                access_modes=['ReadWriteMany'],
                storage_class_name='nfs-csi',
                capacity={
                    'storage': storageCapacity,
                },
                persistent_volume_reclaim_policy='Retain',
                mount_options=['nfsvers=4.1'],
                csi=CSIPersistentVolumeSourceArgs(
                    driver='nfs.csi.k8s.io',
                    volume_handle=f'{nfs_server}#{nfs_path}#{name}',
                    volume_attributes={
                        'server': nfs_server,
                        'share': nfs_path,
                    },
                ),
            ),
        ),
        opts=opts,
    )
    return pv
