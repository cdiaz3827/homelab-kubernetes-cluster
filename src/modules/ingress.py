from pulumi_kubernetes.meta.v1 import ObjectMetaArgs
from pulumi_kubernetes.networking.v1 import (
    HTTPIngressPathArgs,
    HTTPIngressRuleValueArgs,
    Ingress,
    IngressBackendArgs,
    IngressRuleArgs,
    IngressServiceBackendArgs,
    IngressSpecArgs,
    ServiceBackendPortArgs,
)


def K8sIngress(
    name: str,
    metadata: ObjectMetaArgs,
    hostname: str,
    service_name: str,
    service_port_name: str = 'http',
    ingress_class: str = 'traefik',
    opts: ObjectMetaArgs = None,
):
    ingress = Ingress(
        f'{name}_ingress',
        metadata=metadata,
        spec=IngressSpecArgs(
            ingress_class_name=ingress_class,
            rules=[
                IngressRuleArgs(
                    host=hostname,
                    http=HTTPIngressRuleValueArgs(
                        paths=[
                            HTTPIngressPathArgs(
                                path='/',
                                path_type='Prefix',
                                backend=IngressBackendArgs(
                                    service=IngressServiceBackendArgs(
                                        name=service_name,
                                        port=ServiceBackendPortArgs(
                                            name=service_port_name
                                        ),
                                    ),
                                ),
                            )
                        ]
                    ),
                )
            ],
        ),
        opts=opts,
    )
    return ingress
