from pulumi import ResourceOptions
from pulumi_kubernetes.core.v1 import (
    PersistentVolumeClaim,
    PersistentVolumeClaimSpecArgs,
    VolumeResourceRequirementsArgs,
)


def nfs_pvc(
    name: str,
    namespace: str,
    storageRequest: str = '1Gi',
    opts: ResourceOptions = None,
):
    pvc = PersistentVolumeClaim(
        f'{name}_pvc',
        metadata={
            'name': name,
            'namespace': namespace,
        },
        spec=PersistentVolumeClaimSpecArgs(
            storage_class_name='nfs-csi',
            access_modes=['ReadWriteOnce'],
            resources=VolumeResourceRequirementsArgs(
                requests={'storage': storageRequest},
            ),
        ),
        opts=opts,
    )

    return pvc
