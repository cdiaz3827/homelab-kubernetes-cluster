from pulumi import ResourceOptions
from pulumi_kubernetes.core.v1 import Namespace
from pulumi_kubernetes.meta.v1 import ObjectMetaArgs


def K8sNamespace(name: str, opts: ResourceOptions = None):
    ns = Namespace(
        f'{name}_namespace', metadata=ObjectMetaArgs(name=name), opts=opts
    )
    return ns
